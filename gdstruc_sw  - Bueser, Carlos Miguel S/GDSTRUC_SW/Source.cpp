#include <iostream>
#include <string>

using namespace std;

void newScreen()
{
	cout << endl;
	system("pause");
	system("cls");
}

int inputNumber(int& input)
{
	cout << " input a number.\n";
	cin >> input;

	return input;
}

//1. Compute the sum of digits of a number
void computeForSum(int input, int& sum)
{
	if (input == 0)
		return;

	computeForSum(input /= 10, sum += (input % 10));
}

//2. Print the fibonacci numbers up to nth term
void fibonacciSequence(int input, int counter, int pValue, int cValue)
{
	if (counter >= input)
		return;

	cout << pValue << " ";
	int nValue = pValue + cValue;
	pValue = cValue;
	fibonacciSequence(input, counter + 1, pValue, cValue = nValue);
}

//3. Check a number whether it is prime or not
bool checkIfPrime(int inputNumber, int pCounter)
{
	if (inputNumber == 2)
		return true;
	else if (inputNumber % pCounter == 0)
		return false;
	else if (pCounter * pCounter > inputNumber)
		return true;
	else
		return checkIfPrime(inputNumber, pCounter + 1);
}

void main()
{
	int input = 0, sum = 0;

	//1. Compute the sum of digits of a number
	cout << "To get the sum";
	inputNumber(input);

	computeForSum(input, sum);
	cout << "\nThe sum of the digits: " << sum;


	newScreen();
	//2. Print the fibonacci numbers up to nth term
	int counter02 = 0, pValue = 0, cValue = 1;
	cout << "How many sequence to display? please";
	inputNumber(input);

	fibonacciSequence(input, counter02, pValue, cValue);

	newScreen();
	//3. Check a number whether it is prime or not
	int counter03 = 2;
	cout << "(MUST BE POSITIVE!!!), Check if it is a prime number please";
	inputNumber(input);

	if (checkIfPrime(input, counter03))
		cout << input << " is a prime number\n";
	else
		cout << input << " is a not prime number\n";

	newScreen();


	cout << "     Made by BUESER, CARLOS MIGUEL S.     \n          TG003          \n\n     Thank You for Playing";
	newScreen();
}